import React, { useEffect, useState } from "react";
import type { MenuProps } from "antd";
import { AiFillSetting } from "react-icons/ai";
import { Dropdown, Badge, Button, Drawer, Avatar, Image, Select } from "antd";
import { BiBell } from "react-icons/bi";
import { useGetAuth } from "@/lib/react-query/query";
import { getFullName } from "@/utils/app-utils";
import useGlobalStore from "@/store/global-store";
import dayjs from "dayjs";

const items: MenuProps["items"] = [
  {
    label: "English",
    style: {
      display: "flex",
      gap: 5,
    },
    icon: (
      <Image
        src={"/images/english.svg"}
        alt="uk"
        width={26}
        height={22}
        style={{ borderRadius: "6px" }}
        preview={false}
      />
    ),
    key: "english",
  },
  {
    label: "Tiếng Việt",
    style: {
      display: "flex",
      gap: 5,
    },
    icon: (
      <Image
        src={"/images/vietnamese.svg"}
        alt="uk"
        width={26}
        height={22}
        style={{ borderRadius: "6px" }}
        preview={false}
      />
    ),
    key: "vietnamese",
  },
  {
    label: "Français",
    style: {
      display: "flex",
      gap: 5,
    },
    icon: (
      <Image
        src={"/images/france.svg"}
        alt="uk"
        width={26}
        height={22}
        style={{ borderRadius: "6px" }}
        preview={false}
      />
    ),
    key: "france",
  },
];

const Navbar = () => {
  const [lang, setLang] = useState<string>("vietnamese");
  const [open, setOpen] = useState(false);
  const { data: user } = useGetAuth();
  const { masterData, setStartDate, setEndDate , currentBranchId, setCurrentBranchId} = useGlobalStore();

  const [year, setYear] = useState<string>("-2");
  const [month, setMonth] = useState<string>("-2");

  const handleChangeYear = (value: string) => {
    setYear(value);
  };

  const handleChangeMonth = (value: string) => {
    setMonth(value);
  };

  const handleChangeLanguage: MenuProps["onClick"] = (e) => {
    setLang(e.key);
  };

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const hanleChangeBranch = (value: string) => {
    setCurrentBranchId(value);
  };

  useEffect(() => {
    if (Number.parseInt(year) < 0) {
      setMonth("-2");
    }
    const yearInt = Number.parseInt(year);
    const monthInt = Number.parseInt(month);
    let startDate = dayjs("2019-01-01").format("DD/MM/YYYY");
    let endDate = dayjs("2025-01-01").format("DD/MM/YYYY");

    if (yearInt > -1 && monthInt > 0) {
      startDate = dayjs(`${yearInt}-${monthInt}-01`).startOf("month").format("DD/MM/YYYY");
      endDate = dayjs(`${yearInt}-${monthInt}-01`).endOf("month").format("DD/MM/YYYY");
    } else if (yearInt > -1 && monthInt == -1) {
      startDate = dayjs(`${yearInt}-01-01`).startOf("year").format("DD/MM/YYYY");
      endDate = dayjs(`${yearInt}-01-01`).endOf("year").format("DD/MM/YYYY");
    }

    if (yearInt == -1 || (yearInt > 0 && monthInt != -2)) {
      setStartDate(startDate);
      setEndDate(endDate);
  
    }
  }, [year, month]);

  return (
    <div className="z-10 flex w-[calc(100%_-_281px)] justify-between py-4 px-3 border-b border-solid border-[#F0F0F0] fixed top-0 left-auto right-0 h-[64px] backdrop-blur">
      <div className="flex gap-2 items-center">
        <Select
          value={currentBranchId}
          style={{ width: 250 }}
          size="middle"
          onChange={hanleChangeBranch}
          options={[
            { value: "-1", label: "Chọn chi nhánh" },
            ...masterData.branches.map((branch) => {
              return {
                value: branch.id,
                label: branch.name,
              };
            }),
          ]}
        />

        <Select
          value={year}
          style={{ width: 120 }}
          onChange={handleChangeYear}
          options={[
            { value: "-2", label: "Chọn năm" },
            { value: "-1", label: "Tất cả" },
            { value: "2020", label: "2020" },
            { value: "2021", label: "2021" },
            { value: "2022", label: "2022" },
            { value: "2023", label: "2023" },
            { value: "2024", label: "2024" },
          ]}
        />

        <Select
          value={month}
          style={{ width: 120 }}
          onChange={handleChangeMonth}
          disabled={Number.parseInt(year) < 0}
          options={[
            { value: "-2", label: "Chọn tháng" },
            {
              value: "-1",
              label: "Tất cả",
            },
            ...Array(12)
              .fill(null)
              .map((_, idx) => {
                return {
                  value: Number(idx + 1).toString(),
                  label: `Tháng ${idx + 1}`,
                };
              }),
          ]}
        />
      </div>
      <div className="flex gap-3 items-center">
        <Dropdown
          menu={{
            items,
            selectable: true,
            onClick: handleChangeLanguage,
          }}
        >
          <div className="rounded w-[50px]">
            <Image
              src={`/images/${lang}.svg`}
              alt={`lag-${lang}`}
              width={"100%"}
              // height={26}
              // sizes="100%"
              // className="rounded-md"
              style={{ borderRadius: "4px" }}
              preview={false}
            />
          </div>
        </Dropdown>

        <div>
          <Badge count={5}>
            <Button type="text" shape="circle" icon={<BiBell fontSize={22} />} />
          </Badge>
        </div>

        <div>
          <Button onClick={showDrawer} type="text" shape="circle" icon={<AiFillSetting fontSize={22} />} />
          <Drawer
            title="Cài đặt nhanh"
            width={375}
            onClose={onClose}
            open={open}
            placement="right"
            // extra={
            //   <Space>
            //     <Button onClick={onClose}>Cancel</Button>
            //     <Button type="primary" onClick={onClose}>
            //       OK
            //     </Button>
            //   </Space>
            // }
          >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Drawer>
        </div>

        <Button
          className="relative flex gap-1 items-center"
          type="text"
          block
          style={{ paddingLeft: 0, paddingRight: 0 }}
        >
          <Avatar className="mx-auto" size={32} src={user.avatar} />
          <span className="text-[16px]">{getFullName(user.firstName, user.lastName)}</span>
        </Button>
      </div>
    </div>
  );
};

export default Navbar;
