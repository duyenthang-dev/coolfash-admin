import { BASE_URL } from "@/common/app-constant";
import { FullMasterData } from "@/types/master-data";
import axios from "axios";
import { ExpenseDetails, IncomeExpenseProfit } from "../types/response";
import { GetExpenseDetails, GetMonthlyData } from "../types/request";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  // withCredentials: true,
});

export const apiGetAllMasterData = async () => {
  const response = await axiosInstance.get<FullMasterData>("/v1/masterdata");
  return response.data;
};

export const apiGetExpenseDetails = async (requestBody: GetExpenseDetails) => {
  const response = await axiosInstance.post<ExpenseDetails>(
    "/v1/general-reports/expense-details",
    JSON.stringify(requestBody),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data;
};

export const apiGetMonthlyData = async (requestBody: GetMonthlyData) => {
  const response = await axiosInstance.post<IncomeExpenseProfit[]>(
    "/v1/general-reports/get-quota-series",
    JSON.stringify(requestBody),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return response.data;
};
