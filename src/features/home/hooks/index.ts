import { useQuery } from "@tanstack/react-query";
import { apiGetAllMasterData, apiGetExpenseDetails, apiGetMonthlyData } from "../apis";
import { AxiosError } from "axios";
import { AppError } from "@/types";
import useGlobalStore from "@/store/global-store";
import { GetExpenseDetails, GetMonthlyData } from "../types/request";

export const useGetAllMasterData = () => {
  const { setMasterData } = useGlobalStore();
  return useQuery({
    queryKey: ["get_all_master_data"],
    queryFn: async () => {
      try {
        const res = await apiGetAllMasterData();
        setMasterData(res);
        return res;
      } catch (err) {
        if (err instanceof AxiosError) {
          const errDetails = err.response.data;
          throw new AppError(errDetails.status, errDetails.statusCode, errDetails.result);
        }
        throw err;
      }
    },
  });
};

export const useGetExpenseDetails = (request: GetExpenseDetails) => {
  return useQuery({
    queryKey: ["get_expense_details", request],
    queryFn: async () => {
      try {
        const res = await apiGetExpenseDetails(request);
        return res;
      } catch (err) {
        if (err instanceof AxiosError) {
          const errDetails = err.response.data;
          throw new AppError(errDetails.status, errDetails.statusCode, errDetails.result);
        }
        throw err;
      }
    },
    enabled: request != null,
  });
};

export const useGetMonthlYReport = (request: GetMonthlyData) => {
  return useQuery({
    queryKey: ["get_monthly_data", request],
    queryFn: async () => {
      try {
        const res = await apiGetMonthlyData(request);
        return res;
      } catch (err) {
        if (err instanceof AxiosError) {
          const errDetails = err.response.data;
          throw new AppError(errDetails.status, errDetails.statusCode, errDetails.result);
        }
        throw err;
      }
    },
    enabled: request != null,
  });
};
