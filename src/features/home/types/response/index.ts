export interface SummaryIncome {
  startDate: Date;
  endDate: Date;
  branchId: string;
  branchName: string;
  totalExpenses: number;
  totalIncome: number;
  profitsAfterTax: number;
}

export interface QuotaExpense {
  key: string;
  value: number;
  viName: string;
}

export interface ExpenseDetails {
  costTypes: QuotaExpense[];
  costCategories: QuotaExpense[];
}

export interface IncomeExpenseProfit {
  timeline: string;
  income: number;
  expense: number;
  profit: number;
}
