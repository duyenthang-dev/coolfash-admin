export interface GetSummaryIncome {
  branchId: string;
  startDate: string;
  endDate: string;
}

export interface GetExpenseDetails {
  startDate: string;
  endDate: string;
  branchId: string;

  costTypeQuotas?: Quota[];
  costCategoryQuotas?: Quota[];
}

export interface Quota {
  key: string;
  viName: string;
}

export interface GetMonthlyData {
  branchId: string;
  year: number;
  period: "MONTHLY" | "QUARTERLY"
}