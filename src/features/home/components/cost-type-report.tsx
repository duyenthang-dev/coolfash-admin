/* eslint-disable @typescript-eslint/no-unused-vars */
import { useMemo } from "react";
import { useGetExpenseDetails } from "../hooks";
import FullScreenLoader from "@/components/shared/fullscreen-loader";
import useGlobalStore from "@/store/global-store";
import { formatVND } from "@/utils/app-utils";
import { Treemap, Tooltip, ResponsiveContainer } from "recharts";

const COLORS = ["#8889DD", "#9597E4", "#8DC77B", "#A5D297", "#E2CF45", "#F8C12D"];

function CustomizedContent(props: any) {
  const { root, x, y, width, height, index, colors, value, viName } = props;
  const { masterData } = useGlobalStore();
  return (
    <g>
      <rect
        x={x}
        y={y}
        width={width}
        height={height}
        style={{
          fill: colors[Math.floor((index / root.children.length) * 6)],
          stroke: "#fff",
          strokeWidth: 2,
          strokeOpacity: 1,
        }}
      />
      <text x={x + width / 2} y={y + height / 2 + 7} fontWeight={300} textAnchor="middle" fill="#fff" fontSize={14}>
        {formatVND(value)}
      </text>
      <text x={x + 4} y={y + 18} fill="#fff" fontSize={14} fontWeight={300} fillOpacity={0.9}>
        {masterData.costTypes.filter((typ) => typ.name.toLowerCase().includes(viName)).at(0)?.name}
      </text>
    </g>
  );
}

// Custom tooltip component to display viName and value
const CustomTooltip = ({ active, payload }: any) => {
  if (active && payload && payload.length) {
    const { viName, value } = payload[0].payload; // Access viName and value
    return (
      <div className="custom-tooltip" style={{ backgroundColor: "#fff", padding: "10px", border: "1px solid #ccc" }}>
        <p>{`Loại chi phí: ${viName}`}</p>
        <p>{`Giá trị: ${formatVND(value)}`}</p>
      </div>
    );
  }

  return null;
};

const CostTypeReport = () => {
  const { currentBranchId, startDate, endDate } = useGlobalStore();

  const request = useMemo(() => {
    if (startDate == "") return null;
    return {
      branchId: currentBranchId,
      endDate,
      startDate,
    };
  }, [startDate, currentBranchId, endDate]);

  const { isLoading, data: expenseDetails } = useGetExpenseDetails(request);
  const { masterData } = useGlobalStore();

  const costTypes = useMemo(() => {
    if (expenseDetails != null) return expenseDetails.costTypes;
    return null;
  }, [expenseDetails]);

  if (isLoading || costTypes == null) {
    return <FullScreenLoader />;
  }

  return (
    <ResponsiveContainer width="100%" height="100%">
      <Treemap
        // width={400}
        // height={100}
        data={costTypes.sort((a, b) => b.value - a.value).slice(0, 7)}
        dataKey="value"
        stroke="#fff"
        fill="#8884d8"
        content={<CustomizedContent colors={COLORS} />}
      >
        <Tooltip content={<CustomTooltip />} />
      </Treemap>
    </ResponsiveContainer>
  );
};

export default CostTypeReport;
