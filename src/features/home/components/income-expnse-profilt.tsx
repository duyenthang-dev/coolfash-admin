/* eslint-disable @typescript-eslint/no-unused-vars */
import { useMemo } from "react";
import { useGetMonthlYReport } from "../hooks";
import FullScreenLoader from "@/components/shared/fullscreen-loader";
import useGlobalStore from "@/store/global-store";
import { Tooltip, ResponsiveContainer, LineChart, CartesianGrid, XAxis, YAxis, Legend, Line } from "recharts";
import dayjs from "dayjs";
import { GetMonthlyData } from "../types/request";

const formatValue = (value) => {
  const absValue = Math.abs(value);
  if (absValue >= 1000000000) {
    return `${value < 0 ? "-" : ""}${(absValue / 1000000000).toFixed(1)}B`; // Format as billions
  }
  if (absValue >= 1000000) {
    return `${value < 0 ? "-" : ""}${(absValue / 1000000).toFixed(1)}M`; // Format as millions
  }
  return `${value}`; // Return the value as is for smaller numbers
};

const CustomizedDot = (props) => {
  const { cx, cy, stroke, payload, value } = props;
  const backgroundColor = value < 0 ? "#E31D1C" : "#19BC86"; // Red for negative, green for positive
  const textValue = formatValue(value);
  const padding = 4; // Padding around the text
  const rectWidth = textValue.length * 10 + padding; // Width based on text length
  const rectHeight = 24; // Fixed height for the rectangle

  return (
    <g>
      <rect
        x={cx - rectWidth / 2} // Center the rectangle
        y={cy - rectHeight / 2} // Center the rectangle
        width={rectWidth}
        height={rectHeight}
        fill={backgroundColor}
        rx={5} // Rounded corners
      />
      <text x={cx} y={cy} dy={6} fill="#fff" textAnchor="middle" fontSize={14}>
        {textValue} {/* Display formatted value inside the rectangle */}
      </text>
    </g>
  );
};

const CustomTooltip = ({ active, payload }: any) => {
  if (active && payload && payload.length) {
    const { name, value } = payload[0];
    console.log(payload);
    return (
      <div style={{ backgroundColor: "#fff", border: "1px solid #ccc", padding: "10px", borderRadius: "5px" }}>
        <h4>{name}</h4>
        <p>{formatValue(value)}</p>
      </div>
    );
  }
  return null;
};

const IncomeExpenseProfitMonthly = () => {
  const { currentBranchId, startDate, endDate } = useGlobalStore();

  const request = useMemo<GetMonthlyData>(() => {
    if (startDate == "" || endDate == "") return null;
    return {
      branchId: currentBranchId,
      year: dayjs(startDate).year(),
      period: "MONTHLY",
    };
  }, [startDate, currentBranchId, endDate]);

  const { isLoading, data: quotaList } = useGetMonthlYReport(request);
  const { masterData } = useGlobalStore();

  if (isLoading || quotaList == null) {
    return <FullScreenLoader />;
  }

  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart
        width={500}
        height={300}
        data={quotaList}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="timeline" />
        <YAxis tickFormatter={formatValue} />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="income" stroke="#8884d8" activeDot={{ r: 8 }} strokeWidth={3} />
        <Line type="monotone" dataKey="expense" stroke="#FF788F" strokeWidth={3} />
        <Line type="monotone" dataKey="profit" stroke="#82ca9d" strokeWidth={3} dot={<CustomizedDot />} />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default IncomeExpenseProfitMonthly;
