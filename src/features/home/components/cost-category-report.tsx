/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useMemo, useState } from "react";
import { ApexOptions } from "apexcharts";
import Chart from "react-apexcharts";
import { useGetExpenseDetails } from "../hooks";
import FullScreenLoader from "@/components/shared/fullscreen-loader";
import useGlobalStore from "@/store/global-store";
import { formatVND } from "@/utils/app-utils";

const CostCategoryReport = () => {
  const { currentBranchId, startDate, endDate } = useGlobalStore();
  const request = useMemo(() => {
    if (startDate == "") return null;
    return {
      branchId: currentBranchId,
      endDate,
      startDate,
    };
  }, [startDate, currentBranchId, endDate]);

  const { isLoading, data: expenseDetails } = useGetExpenseDetails(request);
  const { masterData } = useGlobalStore();

  const constCategories = useMemo(() => {
    if (expenseDetails != null) return expenseDetails.costCategories;
    return null;
  }, [expenseDetails]);

  const [series, setSeries] = useState([]);
  const [options, setOptions] = useState<ApexOptions>({
    chart: {
      type: "donut",
    },
    labels: [],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  });
  useEffect(() => {
    if (constCategories != null) {
      setSeries(constCategories.map((costType) => costType.value));
      setOptions((prev) => ({
        ...prev,
        labels: constCategories
          .map((costType) => costType.viName)
          .map((name) => masterData.costCategories.filter((cate) => cate.name.toLowerCase().includes(name)).at(0).name),

        tooltip: {
          y: {
            formatter: function (val) {
              return formatVND(val);
            },
          },
        },
      }));
    }
  }, [constCategories, masterData.costCategories]);

  if (isLoading || constCategories == null) {
    return <FullScreenLoader />;
  }
  return (
    <div className="mt-4">
      <Chart type="donut" options={options} series={series} height={350} width={"100%"} />
    </div>
  );
};

export default CostCategoryReport;
