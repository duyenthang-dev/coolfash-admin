import { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { Input, TreeSelect } from "antd";
import UploadArea from "@/components/shared/upload-area";
import useProductStore from "@/store/product-store";
const treeData = [
  {
    value: "clothes",
    title: "Quần áo",
    children: [
      {
        value: "clothes,paint",
        title: "Quần",
        children: [
          {
            value: "clothes,paint,jeans",
            title: "Quần Jeans",
          },
          {
            value: "clothes,paint,dress_paint",
            title: "Quần Âu",
          },
        ],
      },
      {
        value: "clothes,top",
        title: "Áo",
        children: [
          {
            value: "clothes,top,t-shirt",
            title: "Áo T-Shirt",
          },
          {
            value: "clothes,top,sweater",
            title: "Áo Sweater",
          },
        ],
      },
    ],
  },

  {
    value: "shoe",
    title: "Giày",
    children: [
      { value: "shoe,sandals", title: "Dép Sandals" },
      { value: "shoe,sneaker", title: "Giày Sneaker" },
      { value: "shoe,boot", title: "Giày Boots" },
    ],
  },
];

const BasicInfo = () => {
  const [desc, setDesc] = useState("");
  const product = useProductStore().product;
  const { setCategory, setName, setDescription, setImages } = useProductStore();

  const onChange = (newValue: string) => {
    console.log(newValue);
    setCategory(newValue);
  };

  // const onChangeDesc = (va)

  const retrieveUploadedUrls = (data: string[]) => {
    console.log(data);
    setImages(data);
  };

  useEffect(() => {
    setDesc(product.basicInfo.description);
  }, [product.basicInfo.description]);

  return (
    <div>
      <div>
        <label htmlFor="">Tên sản phẩm</label>
        <Input
          size="large"
          placeholder="Nhập vào tên sản phẩm"
          value={product.basicInfo.name}
          className="w-full mt-2"
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
      </div>
      <div className="mt-5 [&_.ql-editor]:min-h-[200px]">
        <label className="inline-block mb-2" htmlFor="prod-desc">
          Mô tả sản phẩm
        </label>
        <ReactQuill
          id="prod-desc"
          theme="snow"
          value={desc}
          onChange={setDesc}
          className="text-editor"
          onBlur={() => {
            setDescription(desc);
          }}
        />
      </div>
      <div className="mt-5">
        <label className="inline-block mb-2" htmlFor="prod-desc">
          Tải ảnh sản phẩm
        </label>
        <UploadArea
          allowedTypes={["jpg"]}
          getResult={retrieveUploadedUrls}
          folderPath="/products/test"
          existingImages={product.basicInfo.images}
        />
      </div>

      <div className="mt-5">
        <label className="inline-block mb-2" htmlFor="prod-desc">
          Phân loại ngành hàng
        </label>
        <TreeSelect
          showSearch
          style={{ width: "100%" }}
          value={product.basicInfo.category}
          dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
          placeholder="Chọn ngành hàng"
          allowClear
          treeDefaultExpandAll
          onChange={onChange}
          treeData={treeData}
        />
      </div>
    </div>
  );
};

export default BasicInfo;
