import type { InputNumberProps } from "antd";
import { InputNumber, Switch } from "antd";
import Card from "@/components/shared/card";
import useProductStore from "@/store/product-store";

const deliveryUnits = ["standard", "economy", "2h"];

const DeliveryInfo = () => {
  const deliveryInfo = useProductStore().product.deliveryInfo;
  const { setProdDimension, setProdWeight, setShippingUnits } = useProductStore();

  const onChangeWeight: InputNumberProps["onChange"] = (value) => {
    setProdWeight(value as number);
  };

  const onChangeWidth: InputNumberProps["onChange"] = (value) => {
    setProdDimension({ dimension: "width", payload: value as number });
  };

  const onChangeLength: InputNumberProps["onChange"] = (value) => {
    setProdDimension({ dimension: "length", payload: value as number });
  };

  const onChangeHeight: InputNumberProps["onChange"] = (value) => {
    setProdDimension({ dimension: "height", payload: value as number });
  };

  const onChangeSwitch = (value: string, checked: boolean) => {
    if (checked) {
      if (!deliveryInfo.shippingUnits.includes(value)) {
        const temp = [...deliveryInfo.shippingUnits, value];
        setShippingUnits(temp);
      }
    } else {
      if (deliveryInfo.shippingUnits.includes(value)) {
        const temp = deliveryInfo.shippingUnits.filter((unit) => unit != value);
        setShippingUnits(temp);
      }
    }
  };

  return (
    <div>
      <h3 className="font-semibold text-xl">Thông tin vận chuyển</h3>
      <div className="mt-5">
        <label className="text-text-soft" htmlFor="product_weight">
          Cân nặng sau khi đã đóng gói (kg)
        </label>
        <InputNumber
          id="product_weight"
          size="large"
          className="w-full mt-2"
          min={0}
          max={10}
          defaultValue={deliveryInfo.weight}
          onChange={onChangeWeight}
        />
      </div>
      <div className="mt-5">
        <label className="text-text-soft" htmlFor="product_length">
          Kích thước đóng gói (cm)
        </label>
        <div className="flex gap-5 mt-2">
          <InputNumber
            id="product_length"
            size="large"
            className="w-full"
            min={0}
            max={10}
            defaultValue={deliveryInfo.length}
            onChange={onChangeLength}
          />
          <InputNumber
            id="product_width"
            size="large"
            className="w-full"
            min={0}
            max={10}
            defaultValue={deliveryInfo.width}
            onChange={onChangeWidth}
          />
          <InputNumber
            id="product_height"
            size="large"
            className="w-full"
            min={0}
            max={10}
            defaultValue={deliveryInfo.height}
            onChange={onChangeHeight}
          />
        </div>
      </div>
      <div className="mt-5">
        <p className="text-text-soft">Phương thức vận chuyển</p>
        {deliveryUnits.map((u, idx) => {
          return (
            <Card key={idx} className="my-3">
              <div className="flex gap-5">
                <p className="w-[200px]">{u.toUpperCase()}</p>
                <InputNumber addonAfter="đ" defaultValue={100} controls />
                <Switch
                  defaultChecked={deliveryInfo.shippingUnits.some((v) => v == u)}
                  onChange={(checked) => {
                    onChangeSwitch(u, checked);
                  }}
                />
              </div>
            </Card>
          );
        })}
      </div>
    </div>
  );
};

export default DeliveryInfo;
