import { BASE_URL } from "@/common/app-constant";
import { SummaryIncome } from "@/features/home/types/response";
import axios from "axios";

const fetcher = axios.create({
  baseURL: BASE_URL,
});

fetcher.defaults.headers.common["Access-Control-Allow-Origin"] = "*";

export const apiGetSummaryIncome = async (request: FormData) => {
  const response = await fetcher.post<SummaryIncome>("v1/general-reports/summary-income", request, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
  return response.data;
};
