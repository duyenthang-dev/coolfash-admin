import { SelectProps } from "antd";
import { MenuItem, IOperationFilter } from "@/types";
import {
  MdOutlineHome,
  MdOutlinePersonOutline,
  MdOutlineSettings,
  MdHelpCenter,
  MdOutlineHomeRepairService,
  MdSecurity,
  MdDashboard,
  MdOutlineCalendarMonth,
  MdOutlineChat,
} from "react-icons/md";

export const menuItems: Array<MenuItem> = [
  {
    title: "Quản lý thu chi",
    list: [
      {
        label: "Báo cáo tổng quan",
        path: "/",
        icon: <MdOutlineHome size={22} />,
      },
      // {
      //   label: "Khoản Thu",
      //   path: "/users",
      //   icon: <MdOutlinePersonOutline fontSize={22} />,
      // },
      // {
      //   label: "Khoản chi",
      //   path: "/products",
      //   icon: <MdOutlineHomeRepairService fontSize={22} />,
      // },
    ],
  },

  {
    title: "Hệ thống",
    list: [
      {
        label: "Phân quyền",
        path: "/authorz",
        icon: <MdSecurity fontSize={22} />,
      },
      {
        label: "Cài đặt",
        path: "/setting",
        icon: <MdOutlineSettings fontSize={22} />,
      },

      {
        label: "Trợ giúp",
        path: "/help",
        icon: <MdHelpCenter fontSize={22} />,
      },
    ],
  },

  {
    title: "Ứng dụng",
    list: [
      {
        label: "Kanban",
        path: "/kanban",
        icon: <MdDashboard />,
      },
      {
        label: "Lịch",
        path: "/calendar",
        icon: <MdOutlineCalendarMonth />,
      },

      {
        label: "Chat",
        path: "/chat",
        icon: <MdOutlineChat />,
      },
    ],
  },
];

export const usersBreadcrumb = [
  {
    href: "/",
    title: <MdOutlineHome fontSize={18} />,
  },

  {
    href: "/users",
    title: "Quản lý người dùng",
  },
  {
    title: "Thêm mới",
  },
];

export const createProductBreadcumbs = [
  {
    href: "/",
    title: <MdOutlineHome fontSize={18} />,
  },

  {
    href: "/products",
    title: "Quản lý sản phẩm",
  },
  {
    title: "Thêm mới",
  },
];

export const genderOptions = [
  { key: "MALE", label: "Nam" },
  { key: "FEMALE", label: "Nữ" },
  { key: "OTHERS", label: "Khác" },
];

export const roleOptions: SelectProps["options"] = [
  {
    label: "Khách hàng",
    value: "BUYER",
    color: "gold",
  },
  {
    label: "Người bán",
    value: "SELLER",
    color: "green",
  },
  {
    label: "Quản trị viên",
    value: "ADMIN",
    color: "lime",
  },

  {
    label: "Nhân viên",
    value: "Staff",
    color: "cyan",
  },
];

export const filterOperations = new Map<string, IOperationFilter[]>();
filterOperations.set("string", [
  {
    label: "Bao gồm",
    value: "%{s}%",
  },
  {
    label: "Bat dau voi",
    value: "%{s}",
  },
  {
    label: "Ket thuc voi",
    value: "{s}%",
  },
  {
    label: "Trong",
    value: "null",
  },
]);

filterOperations.set("number", [
  {
    label: "Lớn hơn",
    value: ">",
  },
  {
    label: "Nhỏ hơn",
    value: "<",
  },
  {
    label: "Bằng",
    value: "=",
  },
  {
    label: "Khác",
    value: "<>",
  },
]);
