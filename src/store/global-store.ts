import { FullMasterData, GlobalStoreState, InitGlobalStoreState } from "@/types/master-data";
import { create } from "zustand";

const initialState: InitGlobalStoreState = {
  masterData: {
    branches: [],
    costCategories: [],
    costTypes: [],
    incomeTypes: [],
  },
  currentBranchId: "-1",
  startDate: "",
  endDate: "",
};

const useGlobalStore = create<GlobalStoreState>((set) => ({
  masterData: initialState.masterData,
  currentBranchId: initialState.currentBranchId,
  startDate: initialState.startDate,
  endDate: initialState.endDate,

  setMasterData: (payload: FullMasterData) => {
    return set((state) => ({
      ...state,
      masterData: payload,
    }));
  },

  setCurrentBranchId: (payload: string) => {
    return set((state) => ({
      ...state,
      currentBranchId: payload,
    }));
  },

  setStartDate: (payload: string) => {
    return set((state) => ({
      ...state,
      startDate: payload,
    }));
  },

  setEndDate: (payload: string) => {
    return set((state) => ({
      ...state,
      endDate: payload,
    }));
  },
}));

export default useGlobalStore;
