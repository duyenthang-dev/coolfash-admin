import { VariantInfo, VariantType } from "@/features/product/components/sell-info";
import { create } from "zustand";

export interface ProdBasicInfo {
  id: string;
  name: string;
  description: string;
  images: string[];
  category: string;
}

export interface ProdSellingInfo {
  hasVariant: boolean;
  productionVariants?: Array<VariantType>;
  variantInfo: Array<VariantInfo>;
  price?: number;
  stock?: number;
}

export interface ProdDeliveryInfo {
  weight: number;
  length?: number;
  width?: number;
  height?: number;
  shippingUnits: string[];
}

export interface Product {
  basicInfo: ProdBasicInfo;
  sellingInfo: ProdSellingInfo;
  deliveryInfo: ProdDeliveryInfo;
}

export interface ProductStoreState {
  product: Product;

  setName: (payload: string) => void;
  setDescription: (payload: string) => void;
  setImages: (payload: string[]) => void;
  setCategory: (payload: string) => void;
  setId: (payload: string) => void;

  setHasVariant: (payload: boolean) => void;
  setPrice: (payload: number) => void;
  setStock: (payload: number) => void;
  setProductVariants: (payload: VariantType[]) => void;
  setVariantInfo: (payload: VariantInfo[]) => void;

  setProdWeight: (payload: number) => void;
  setProdDimension: ({ dimension, payload }: { payload: number; dimension: string }) => void;
  setShippingUnits: (payload: string[]) => void;
}

const initialState: Product = {
  basicInfo: {
    id: "",
    name: "",
    description: "",
    images: [],
    category: "",
  },
  sellingInfo: {
    hasVariant: false,
    productionVariants: [],
    price: 0,
    stock: 0,
    variantInfo: [],
  },
  deliveryInfo: {
    weight: 0,
    length: 0,
    width: 0,
    height: 0,
    shippingUnits: [],
  },
};

const useProductStore = create<ProductStoreState>((set) => ({
  product: initialState,

  setName: (payload: string) =>
    set((state) => ({
      ...state,
      product: { ...state.product, basicInfo: { ...state.product.basicInfo, name: payload } },
    })),

  setId: (payload: string) =>
    set((state) => ({
      ...state,
      product: { ...state.product, basicInfo: { ...state.product.basicInfo, id: payload } },
    })),

  setImages: (payload: string[]) =>
    set((state) => ({
      ...state,
      product: { ...state.product, basicInfo: { ...state.product.basicInfo, images: payload } },
    })),

  setDescription: (payload: string) =>
    set((state) => ({
      ...state,
      product: { ...state.product, basicInfo: { ...state.product.basicInfo, description: payload } },
    })),

  setCategory: (payload: string) =>
    set((state) => ({
      ...state,
      product: { ...state.product, basicInfo: { ...state.product.basicInfo, category: payload } },
    })),

  setHasVariant: (payload: boolean) => {
    console.log("hgaha", payload);
    return set((state) => ({
      ...state,
      product: { ...state.product, sellingInfo: { ...state.product.sellingInfo, hasVariant: payload } },
    }));
  },

  setPrice: (payload: number) =>
    set((state) => ({
      ...state,
      product: { ...state.product, sellingInfo: { ...state.product.sellingInfo, price: payload } },
    })),

  setStock: (payload: number) =>
    set((state) => ({
      ...state,
      product: { ...state.product, sellingInfo: { ...state.product.sellingInfo, stock: payload } },
    })),

  setProductVariants: (payload: VariantType[]) =>
    set((state) => ({
      ...state,
      product: { ...state.product, sellingInfo: { ...state.product.sellingInfo, productionVariants: payload } },
    })),

  setVariantInfo: (payload: VariantInfo[]) =>
    set((state) => ({
      ...state,
      product: { ...state.product, sellingInfo: { ...state.product.sellingInfo, variantInfo: payload } },
    })),

  setProdWeight: (payload: number) =>
    set((state) => ({
      ...state,
      product: { ...state.product, deliveryInfo: { ...state.product.deliveryInfo, weight: payload } },
    })),

  setProdDimension: ({ dimension, payload }: { payload: number; dimension: string }) => {
    switch (dimension) {
      case "height":
        return set((state) => ({
          ...state,
          product: { ...state.product, deliveryInfo: { ...state.product.deliveryInfo, height: payload } },
        }));
      case "length":
        return set((state) => ({
          ...state,
          product: { ...state.product, deliveryInfo: { ...state.product.deliveryInfo, length: payload } },
        }));
      case "width":
        return set((state) => ({
          ...state,
          product: { ...state.product, deliveryInfo: { ...state.product.deliveryInfo, width: payload } },
        }));
    }
  },

  setShippingUnits: (payload: string[]) =>
    set((state) => ({
      ...state,
      product: { ...state.product, deliveryInfo: { ...state.product.deliveryInfo, shippingUnits: payload } },
    })),
}));

export default useProductStore;
