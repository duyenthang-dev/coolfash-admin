export const getFullName = (fname: string, lname: string): string => {
  return fname + " " + lname;
};

export const formatVND = (value: any) => {
  value = value == null ? 0 : value;
  return value.toLocaleString("vi-VN", { style: "currency", currency: "VND" });
};
