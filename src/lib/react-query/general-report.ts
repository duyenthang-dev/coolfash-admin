import { apiGetSummaryIncome } from "@/apis/general-report";
import { GetSummaryIncome } from "@/features/home/types/request";
import { AppError } from "@/types";
import { useQuery } from "@tanstack/react-query";
import { AxiosError } from "axios";

export const useGetSummaryIncome = (data: GetSummaryIncome) => {
  return useQuery({
    queryKey: [`get_summary_income`, data],
    queryFn: async () => {
      try {
        const formData = new FormData();
        formData.append("start_date", data.startDate);
        formData.append("end_date", data.endDate);
        formData.append("branch_id", data.branchId);

        return await apiGetSummaryIncome(formData);
      } catch (err) {
        if (err instanceof AxiosError) {
          const errDetails = err.response.data;
          throw new AppError(errDetails.status, errDetails.statusCode, errDetails.result);
        }
        throw new Error(err.message);
      }
    },
    enabled: data != null,
  });
};
