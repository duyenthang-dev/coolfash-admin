export interface BranchResponse {
  id: string;
  name: string;
}

export interface IncomeTypeResponse {
  id: string;
  name: string;
}

export interface CostTypeResponse {
  id: string;
  name: string;
  cashflow: string;
  costCategoryId: string;
}

export interface CostCategoryResponse {
  id: string;
  name: string;
}

export interface FullMasterData {
  branches: BranchResponse[];
  incomeTypes: IncomeTypeResponse[];
  costTypes: CostTypeResponse[];
  costCategories: CostCategoryResponse[];
}

export interface InitGlobalStoreState {
  masterData: FullMasterData;
  currentBranchId: string;
  startDate: string;
  endDate: string;
}

export interface GlobalStoreState extends InitGlobalStoreState {
  setMasterData: (payload: FullMasterData) => void;
  setCurrentBranchId: (payload: string) => void;
  setStartDate: (payload: string) => void;
  setEndDate: (payload: string) => void;
}
