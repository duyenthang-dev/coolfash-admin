import { ReactNode } from "react";

export interface GeneralModel<T> {
  get id(): T;
}

export class GeneralColumnType {
  key: React.Key;
  action: ReactNode;
}

export interface IRole extends GeneralModel<string> {
  name: string;
  description: string;
}

export interface IAddress {
  street: string;
  ward: string;
  district: string;
  country: string;
  zipcode: number;
  representName: string;
  phoneNumber: string;
  default: boolean;
}

export interface IUser extends GeneralModel<string> {
  active: boolean;
  avatar: string;
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  birthday: Date;
  phoneNumber: string;
  gender: string;
  roles: IRole[];
  addressList: Array<IAddress>;
}

export interface IProduct extends GeneralModel<string> {
  name: string;
  description: string;
  image: string;
  price: number;
  discountPrice?: number;
  category: string;
  stock: number;
  soldCount: number;
  merchantName: string;
}

export interface ILoginResponse {
  token: string;
  refreshToken: string;
  user: IUser;
}

export interface IOperationFilter {
  value: string;
  label: string;
}

export interface IColFilter {
  value: string;
  label: string;
  type: string;
}

export interface FilterItemData {
  column: IColFilter;
  operator: IOperationFilter;
  value?: any;
}

export type MenuItem = {
  title: string;
  list: Array<{
    label: string;
    path: string;
    icon: React.ReactNode;
  }>;
};

export interface GeneralApiResponse<T> {
  statusCode: string;
  status: number;
  result: T;
}

export interface PageResponse<T> extends GeneralApiResponse<T> {
  sort: any;
  pageSize: number;
  pageNumber: number;
  totalElements: number;
  totalPages: number;
  empty: boolean;
  hasNext: boolean;
  hasPrevious: boolean;
}

export type GetQueryParams = {
  orders: string;
  searchText: string;
  pageSize: number;
  pageIndex: number;
};

export class AppError extends Error {
  status: number;
  statusCode: string;
  constructor(status: number, statusCode: string, message: string) {
    super(message);
    this.status = status;
    this.name = "App Error";
    this.statusCode = statusCode;
  }
}
