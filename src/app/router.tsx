/* eslint-disable react-refresh/only-export-components */
import AuthLayout from "@/app/auth/auth-layout";
import DashboardLayout from "@/app/dashboard/dashboard-layout";
import FreeLayout from "@/app/maintainance/free-layout";
import Guard from "@/components/guard";
import FullScreenLoader from "@/components/shared/fullscreen-loader";
import { Suspense, lazy } from "react";
import type { RouteObject } from "react-router-dom";

const Loadable = (Component: React.ComponentType<unknown>) => (props: JSX.IntrinsicAttributes) =>
  (
    <Suspense fallback={<FullScreenLoader />}>
      <Component {...props} />
    </Suspense>
  );

const LoginPage = Loadable(lazy(() => import("@/app/auth/pages/login")))
const RegisterPage = Loadable(lazy(() => import("@/app/auth/pages/register")));

const Forbidden = Loadable(lazy(() => import("@/app/auth/pages/403")));
const NotFound = Loadable(lazy(() => import("@/app/maintainance/pages/404")));
const ServiceUnavailable = Loadable(lazy(() => import("@/app/maintainance/pages/500")));

const UserManagement = Loadable(lazy(() => import("@/app/dashboard/pages/user/user")));
const CreateUpdateUser = Loadable(lazy(() => import("@/app/dashboard/pages/user/create-update-user")));

const ProductManagement = Loadable(lazy(() => import("@/app/dashboard/pages/product/product")))
const CreateProductPage = Loadable(lazy(() => import("@/app/dashboard/pages/product/create")))

const Home = Loadable(lazy(() => import("@/app/dashboard/pages/home")));

const authRoutes: RouteObject = {
  element: <AuthLayout />,
  children: [
    {
      path: "login",
      element: <LoginPage />,
    },
    {
      path: "register",
      element: <RegisterPage />,
    },
    {
      path: "unauthorized",
      element: <Forbidden />,
    },
  ],
};

const dashboardRoute: RouteObject = {
  element: <DashboardLayout />,
  children: [
    {
      path: "",
      element: <Guard canAccess={["SuperAdmin".toUpperCase()]} />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/users",
          element: <UserManagement />,
        },
        {
          path: "/user/new",
          element: <CreateUpdateUser />,
        },
        {
          path: "/user/edit",
          element: <CreateUpdateUser />
        },
        {
          path: "/products",
          element: <ProductManagement />
        },
        {
          path: "/products/create",
          element: <CreateProductPage />
        }
      ],
    },
  ],
};

const maintainanceRoute: RouteObject = {
  element: <FreeLayout />,
  children: [
    {
      path: "/service-unavailable",
      element: <ServiceUnavailable />,
    },
  ],
};

const notFound: RouteObject = {
  path: "*",
  element: <NotFound />,
};

const routes: RouteObject[] = [authRoutes, dashboardRoute, maintainanceRoute, notFound];

export default routes;
