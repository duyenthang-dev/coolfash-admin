import CostCategoryReport from "@/features/home/components/cost-category-report";
import Card from "@/components/shared/card";
import { useGetSummaryIncome } from "@/lib/react-query/general-report";
import { formatVND } from "@/utils/app-utils";
import { Col, Row } from "antd";
import { BsCashStack } from "react-icons/bs";
import CostTypeReport from "@/features/home/components/cost-type-report";
import useGlobalStore from "@/store/global-store";
import { useMemo } from "react";
import IncomeExpenseProfitMonthly from "@/features/home/components/income-expnse-profilt";

const HomePage = () => {
  const { startDate, currentBranchId, endDate } = useGlobalStore();
  const request = useMemo(() => {
    if (startDate == "") return null;
    return {
      branchId: currentBranchId,
      endDate,
      startDate,
    };
  }, [startDate, currentBranchId, endDate]);
  const { data: summaryIncome } = useGetSummaryIncome(request);

  return (
    <div>
      <section>
        <div className="flex justify-between items-center">
          <h2 className="pb-8 text-2xl font-semibold">
            Tổng quan thu chi từ {startDate} tới {endDate}
          </h2>
          <div className="flex gap-2"></div>
        </div>

        <div className="sale-overview flex gap-4">
          <div className="flex-1">
            <Card className={"flex-col items-center py-10 green-gradient text-[#004B50_!important]"}>
              <BsCashStack className="green-gradient inline-block w-[100%]" fontSize={42} />
              <h3 className="text-3xl font-extrabold mb-2 mt-5 mx-auto text-center">
                {formatVND(summaryIncome?.totalIncome)}
              </h3>
              <p className="font-medium mx-auto text-center">Tiền thu về</p>
            </Card>
          </div>
          <div className="flex-1">
            <Card className={"flex-col items-center py-10 blue-gradient text-[#003768_!important]"}>
              <BsCashStack className="blue-gradient inline-block w-[100%]" fontSize={42} />
              <h3 className="text-3xl font-extrabold mb-2 mt-5 text-center">
                {formatVND(summaryIncome?.totalExpenses)}
              </h3>
              <p className="font-medium mx-auto text-center">Tổng chi phí</p>
            </Card>
          </div>

          <div className="flex-1">
            <Card className={"flex-col items-center py-10 orange-gradient text-[#7A4100_!important]"}>
              <BsCashStack className="orange-gradient inline-block w-[100%]" fontSize={42} />
              <h3 className="text-3xl font-extrabold mb-2 mt-5 text-center">
                {formatVND(summaryIncome?.profitsAfterTax)}
              </h3>
              <p className="font-medium mx-auto text-center">Lợi nhuận sau thuế</p>
            </Card>
          </div>
        </div>

        <div className="mt-5">
          <Row gutter={12}>
            <Col span={9}>
              <Card className="h-[510px]">
                <div className="flex justify-between items-center">
                  <h1 className="text-xl font-semibold">Cơ cấu chi phí</h1>
                </div>
                <CostCategoryReport />
              </Card>
            </Col>
            <Col span={15}>
              <Card>
                <h4 className="font-semibold text-xl mb-4">Cơ cấu chi phí </h4>
                <div className="h-[415px] max-h-full">
                  <CostTypeReport />
                </div>
              </Card>
              {/* <div className="mb-3">
              <MonthlySale />
            </div>
            <div>
              <MonthlyTraffic />
            </div> */}
            </Col>
          </Row>
        </div>
      </section>

      <section className="mt-10">
        <div className="flex justify-between items-center">
          <h2 className="pb-8 text-2xl font-semibold">Doanh Thu, Chi phí, Lợi nhuận</h2>
        </div>

        <Row gutter={12}>
          <Col span={24}>
            <div className="h-[500px] w-full">
              <IncomeExpenseProfitMonthly />
            </div>
          </Col>
        </Row>
      </section>
    </div>
  );
};

export default HomePage;
