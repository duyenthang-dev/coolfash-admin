export { default as HomePage } from "./home";
export { default as UserManagement } from "./user/user";
export { default as CreateProduct } from "./product/create";
